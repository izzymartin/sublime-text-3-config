# Sublime Text 3 Configuration

Sublime Text 3 configuration files.

## About

This project was created to make it easy for me to update and migrate my Sublime Text 3 configuration files to various computers.

## Author

* **Isabella Martin** - [IzzyMartin](https://gitlab.com/IzzyMartin)

## License

This project is licensed under the GNU GPL v3 License - see the [LICENSE.md](LICENSE.md) file for details